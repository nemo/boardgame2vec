@0x8c474a82150f9ee5;

enum CardType {
  chopstick @0;
  dumpling @1;
  maki @2;
  nigiri @3;
  pudding @4;
  sashimi @5;
  tempura @6;
  wasabi @7;
}

enum Desserts {
  pudding @0;
}

enum PlayerCardLocations {
  hand @0;
  meal @1;
  carry @2;
}

struct Location {

  union {
    deck :group {
      deck @0 :Bool = true;
    }

    player :group {
      # Which player
      index @1 :UInt8 = 0;
      
      location @2 :PlayerCardLocations;
    }
  }
}

struct Card {
  type @0 :CardType;
  location @1 :Location;
}

struct State {
  cardlist @0 :List(Card);
  # 1/2/3
  round @1 :UInt8 = 1;
}