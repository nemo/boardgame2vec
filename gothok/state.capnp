@0xdcf6fa9421875ce0;

enum House {
  stark @8;
  greyjoy @7;
  lannister @6;
  targaryen @5;
  baratheon @4;
  tyrell @3;
  tully @2;
  varys @1;
  empty @0;
}

struct Location {
  union {
    # Is card on the board
    board :group {
      x @0 :UInt8 = 0;
      y @1 :UInt8 = 0;
    }
    # Or held by a player
    player :group {
      index @2 :UInt8 = 0;
    }
  }
}

struct Card {
  house @0 :House = empty;
  location @1 :Location;
}

struct State {
  cardlist @0 :List(Card);
}
