# A Game of Thrones: Hand of the King

See [rules][rules]. This is a very toned down version of the game, without the companion cards as that reduces the state space by a lot (since all house cards can be treated as identical)

Card lists can be found at the following 2 images:

- [Characters](https://boardgamegeek.com/image/3687891/game-thrones-hand-king?size=original)
- [Companions](https://cf.geekdo-images.com/images/pic3253090.jpg)

## Setup:

1. Shuffle the following string: SSSSSSSSGGGGGGGLLLLLLTTTTTBBBBYYYUUV
2. Fit it into a 6x6 grid

## Play:

1. Check for valid moves (Max = 10) from (5 in east/west and 5 in north/south directions).
2. Declare for a house and move Varys to the corresponding character. This must be the farthest card for that house in that direction.
3. Collect the card, and gain the banner token for that house if you have the highest number of cards for that House (This can be made simpler by giving you the token if you have a proven majority)

## Score

If there are no legal moves left in the game, check number of tokens held by each player. Highest banner token player wins.

See my previous python implementation of the game at https://github.com/captn3m0/gothok/
