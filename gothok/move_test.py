import game
import capnp
import state_capnp

f = open('state.bin', 'rb')
initial_state = state_capnp.State.read_packed(f)
s = game.State(initial_state)

# BTBYSL
# LGSYSG
# GTSSSB
# UUT*TG
# LTLGSB
# YGLLSG


s.display()

possible_moves = set(s.getPossibleMoves())
print(possible_moves)

assert(possible_moves == set([(3, 0), (0, 3), (3, 4), (4, 3)]))
