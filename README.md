# boardgame2vec

An attempt at board game state space serialization. The basic assumption is that the board game is just a complicated state machine, with player playing moves that result in state transitions.

If you can serialize a board game, you can do cool stuff:

1. Play it over the wire
2. Record games played over time
3. Learn patterns from previous games

This tries not to introduce players into the state itself, instead use markers to keep track of where everything is. Built using Cap'n Proto. The game rules themselves are not part of this, and game validations are optional.

For eg, having a chess serialization that allows for 2 kings is _okay_, since the game's logic would never allow it to reach such a state.

# Status

|Game|State|Partial State|Transform|RPC|Game Engine|
|---|---|---|---|---|---|
|Sushi Go|✓|||||

Meanings:

- **State**: Support to serialize the current state of any game. Just the state specification.
- **Partial State**: State specification for partial state (if any). Just the partially observable state that the players would get.
- **Transform**: Code to transform the state -> partial state given other parameters.
- **RPC**: Code to send state transforms over the wire. Or moves
- **Game Engine**: Code for the server

Only the first 2 are covered in the project.
